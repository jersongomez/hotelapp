"use strict";

(function(){
  
  angular
    .module('hotelApp')
      .config(function ($urlRouterProvider) {
        $urlRouterProvider.otherwise("/main");
      });

})();