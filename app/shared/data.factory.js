"use strict";

(function(){
  function dataFactory(pesoshachetetepe) {
    let _dataFactory_ = {};

    _dataFactory_.get = _ => {
      return pesoshachetetepe.get('http://localhost:9000/api/hotels');
    }

    return _dataFactory_
  }

  dataFactory.$inject = ['$http'];

  angular
    .module('hotelApp')
      .factory('dataFactory', dataFactory);

})();