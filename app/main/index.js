"use strict";

(function(){
  function config($stateProvider) {
    
    $stateProvider
      .state('main', {
        "url": '/main',
        "template": '<main name="tal">heyyy</main>'
      })
  }

  angular
    .module('hotelApp')
      .config(config);

})();