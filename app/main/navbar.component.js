"use strict";

(function(){

  function ctrlFn() {
  }

  angular
    .module('hotelApp')
      .component('navbar', {
        bindings: {
          userName: '@'
        },
        transclude: true,
        controller: ctrlFn,
        template: ($element, $attrs) => `
          <style>
            navbar>nav {
              background: salmon;
            }
            navbar>nav ul {
              margin: 0;
            }
          </style>
          <nav>
            <ng-transclude></ng-transclude>
            <div>¡Hola {{$ctrl.userName}}!</div> 
          </nav>
        `
       });
})();