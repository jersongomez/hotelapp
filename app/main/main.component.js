"use strict";

(function(){

  function ctrlFn(dataFactory) {
    this.$onInit = _ => {
      let promise = dataFactory.get();
      promise.then(data => {this.hoteles = data.data});
    }
  }
  ctrlFn.$inject = ['dataFactory'];

  angular
    .module('hotelApp')
      .component('main', {
        bindings: {
          name: '@'
        },
        transclude: true,
        controller: ctrlFn,
        template: ($element, $attrs) => `
          <div class="hotels-container>
            <div class="hotel" ng-repeat="hotel in $ctrl.hoteles">
              {{hotel}}
            </div>
          </div>
        `
       });
})();